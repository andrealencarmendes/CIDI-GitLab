# CIDI GitLab

Arquivo simples para uso do git 


<pre> 
image: java:8

stages:
   - build
   - test
   - deploy

before_script:
  - chmod +x mvnw

build:
  stage: build
  script: ./mvnw package
  artifacts: 
    paths: 
      - target/$artifactId.jar

test:
  stage: test
  script: ./mvnw test

quality:
  stage: test
  script: ./mvnw -Pprod clean verify sonar:sonar -Dsonar.host.url=$HOST -Dsonar.login=$LOGIN -Dsonar.password=$PASSWORD

deploy:
  stage: deploy
  script:
    - echo "Iniciando o deploy ....."
    - git remote add heroku https://heroku:$API-TOKEN@git.heroku.com/$Projeto
    - git push -f heroku HEAD:master
    - echo "deploy sendo feito no herok"
</pre>
